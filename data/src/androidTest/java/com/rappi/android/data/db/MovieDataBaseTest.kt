package com.rappi.android.data.db

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.rappi.android.data.db.dao.MovieWithResultWrapperDAO
import com.rappi.android.data.db.entities.MovieListEntity
import com.rappi.android.domain.enums.MovieCategory
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.IOException

class MovieDataBaseTest {
    private lateinit var movieDao: MovieWithResultWrapperDAO
    private lateinit var db: MovieDataBase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, MovieDataBase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        movieDao = db.movieWithResultWrapperDAO()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetNight() {
        val movie = MovieListEntity()
        movieDao.insertMovieList(movie)
        val movieList = movieDao.getMovie(-1)
        assertEquals(movieList.movieListEntity.category, MovieCategory.UNKNOWN)
    }

}