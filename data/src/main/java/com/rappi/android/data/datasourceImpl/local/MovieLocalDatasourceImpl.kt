package com.rappi.android.data.datasourceImpl.local

import androidx.paging.LoadType
import com.rappi.android.data.db.dao.MovieWithResultWrapperDAO
import com.rappi.android.data.mappers.toMovieListDomain
import com.rappi.android.domain.datasource.local.MovieLocalDataSource
import com.rappi.android.domain.models.MovieListModel
import com.rappi.android.domain.networkObjects.MovieList
import javax.inject.Inject

class MovieLocalDatasourceImpl @Inject constructor(private val movieWithResultWrapperDAO: MovieWithResultWrapperDAO) :
    MovieLocalDataSource {

    override suspend fun saveMoviePaging(
        movieCategory: String,
        movieList: MovieList,
        loadTye: LoadType,
        page: Int,
        endOfPaginationReached: Boolean
    ) {
        movieWithResultWrapperDAO.saveMoviePaging(
            movieCategory,
            movieList,
            loadTye,
            page,
            endOfPaginationReached
        )
    }


    override suspend fun getRemoteKeys(movieId: Int): MovieListModel? {
        return movieWithResultWrapperDAO.getMovieListPaging(movieId)?.toMovieListDomain()
    }
}