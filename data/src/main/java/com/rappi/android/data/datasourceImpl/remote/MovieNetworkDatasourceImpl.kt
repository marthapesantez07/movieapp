package com.rappi.android.data.datasourceImpl.remote

import com.rappi.android.data.apiService.ApiService
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.networkObjects.MovieList
import javax.inject.Inject

class MovieNetworkDatasourceImpl @Inject constructor(private val apiService: ApiService) :
    MovieNetworkDatasource {
    //paging
    override suspend fun getPopularMoviePaging(page: Int): MovieList? {
        return apiService.getPopularMoviePaging(page)
    }

    override suspend fun getTopRatePaging(page: Int): MovieList? {
        return apiService.getTopRateMoviePaging(page)
    }

    override suspend fun getSearchMoviePaging(query: String, page: Int): MovieList? {
        return apiService.getSearchMoviePaging(query, page)
    }
}