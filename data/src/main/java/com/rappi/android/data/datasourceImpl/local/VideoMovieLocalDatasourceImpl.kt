package com.rappi.android.data.datasourceImpl.local

import android.util.Log
import com.rappi.android.data.db.dao.VideoMovieDAO
import com.rappi.android.data.mappers.toListVideoEntity
import com.rappi.android.data.mappers.toVideoListModel
import com.rappi.android.domain.datasource.local.VideoMovieLocalDataSource
import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Completable
import io.reactivex.Maybe
import javax.inject.Inject

class VideoMovieLocalDatasourceImpl @Inject constructor(private val videoMovieDAO: VideoMovieDAO) :
    VideoMovieLocalDataSource {
    override fun getVideoByMovieIdLocal(movieId: Int): Maybe<VideoListModel> {
        return videoMovieDAO.getVideoByMovie(movieId).map {
            it.toVideoListModel(movieId)
        }
    }

    override fun saveVideoByMovie(movieId: Int, videoList: VideoList): Completable {
        return Completable.create { emitter ->
            videoMovieDAO.deleteVideoByMovie(movieId).subscribe({
                videoMovieDAO.insertVideoResult(
                    videoList.results.toListVideoEntity(
                        movieId
                    )
                ).subscribe({
                    Log.i("saveVideoMovie", " -> Success")
                    emitter.onComplete()
                }, { e ->
                    Log.e("saveVideoMovie", e.message.orEmpty())
                    emitter.tryOnError(e)
                }).dispose()
            }, { exception ->
                emitter.tryOnError(exception)
            }).dispose()
        }
    }
}