package com.rappi.android.data.db.converts

import androidx.room.TypeConverter
import com.rappi.android.domain.enums.MovieCategory

open class ConvertDb {
    @TypeConverter
    fun fromStringToMovieCategory(value: String): MovieCategory {
        return MovieCategory.getCategory(value)
    }

    @TypeConverter
    fun movieCategoryToString(movieCategory: MovieCategory): String {
        return movieCategory.name
    }
}