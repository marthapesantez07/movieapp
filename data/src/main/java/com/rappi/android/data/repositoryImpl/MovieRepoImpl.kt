package com.rappi.android.data.repositoryImpl

import androidx.paging.*
import com.rappi.android.data.datasourceImpl.Constants.Companion.DEFAULT_PAGE_SIZE
import com.rappi.android.data.datasourceImpl.MovieMediator
import com.rappi.android.data.datasourceImpl.SearchMoviePagingSource
import com.rappi.android.data.db.dao.MovieResultDAO
import com.rappi.android.data.mappers.toModelResult
import com.rappi.android.data.mappers.toMovieListResultModel
import com.rappi.android.domain.datasource.local.MovieLocalDataSource
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.repository.MovieRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieRepoImpl @Inject constructor(
    private val movieNetworkDatasource: MovieNetworkDatasource,
    private val movieLocalDataSource: MovieLocalDataSource,
    private val movieResultDAO: MovieResultDAO
) : MovieRepo {
    /*

    */
    ///paging
    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(
            pageSize = DEFAULT_PAGE_SIZE,
            enablePlaceholders = false,
            prefetchDistance = 5,
            //maxSize = 30,
            //initialLoadSize = 40
        )
    }

    @ExperimentalPagingApi
    override fun getMoviePaging(category: String): Flow<PagingData<MovieResultModel>> {
        val pagingConfig: PagingConfig = getDefaultPageConfig()
        return Pager(
            config = pagingConfig,
            remoteMediator = MovieMediator(
                movieLocalDataSource,
                movieNetworkDatasource,
                category
            ),
            pagingSourceFactory = {
                movieResultDAO.getAllMoviePaging(category)
            },
        ).flow.map { it ->
            it.map { it1 ->
                it1.toModelResult()
            }
        }
    }

    override fun getSearchMoviePaging(
        category: String,
        query: String
    ): Flow<PagingData<MovieResultModel>> {
        val pagingConfig: PagingConfig = getDefaultPageConfig()
        return Pager(
            config = pagingConfig,
            pagingSourceFactory = {
                SearchMoviePagingSource(movieNetworkDatasource, query = query)
            },
        ).flow.map { it ->
            it.map { it1 ->
                it1.toMovieListResultModel()
            }
        }
    }

}