package com.rappi.android.data.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import com.rappi.android.data.db.entities.MovieListEntity
import com.rappi.android.data.db.entities.MovieResultEntity

@Dao
interface MovieResultDAO {

    @Query("SELECT r.* FROM ${MovieResultEntity.TABLE_NAME} r, ${MovieListEntity.TABLE_NAME} m WHERE r.idMovieList=m.idMovieList and category=:category")
    fun getAllMoviePaging(
        category: String
    ): PagingSource<Int, MovieResultEntity>
}