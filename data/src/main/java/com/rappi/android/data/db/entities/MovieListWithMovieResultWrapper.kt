package com.rappi.android.data.db.entities

import androidx.room.Embedded
import androidx.room.Relation

data class MovieListWithMovieResultWrapper(
    @Embedded
    var movieListEntity: MovieListEntity = MovieListEntity(),
    @Relation(
        entity = MovieResultEntity::class,
        parentColumn = "idMovieList",
        entityColumn = "idMovieList"
    )
    var results: List<MovieResultWithVideoWrapper> = listOf()
)