package com.rappi.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = MovieResultEntity.TABLE_NAME,
)
data class MovieResultEntity(
    @PrimaryKey
    var idMovie: Int = -1,
    var idMovieList: Long = 0,
    var posterPath: String? = null,
    var adult: Boolean = false,
    var overview: String = "",
    var releaseDate: String = "",
    var originalTitle: String = "",
    var originalLanguage: String = "",
    var title: String = "",
    var backdropPath: String? = null,
    var popularity: Double = 0.toDouble(),
    var voteCount: Int = -1,
    var video: Boolean = false,
    var voteAverage: Double = 0.toDouble()
) {
    companion object {
        const val TABLE_NAME = "MovieResultEntity"
    }
}