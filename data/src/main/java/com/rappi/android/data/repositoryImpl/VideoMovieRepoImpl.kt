package com.rappi.android.data.repositoryImpl

import android.util.Log
import com.rappi.android.data.mappers.toMovieListModel2
import com.rappi.android.domain.datasource.local.VideoMovieLocalDataSource
import com.rappi.android.domain.datasource.remote.VideoMovieNetworkDataSource
import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.MovieResult
import com.rappi.android.domain.repository.VideoMovieRepo
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class VideoMovieRepoImpl @Inject constructor(
    private val videoMovieLocalDataSource: VideoMovieLocalDataSource,
    private val videoMovieNetworkDataSource: VideoMovieNetworkDataSource
) : VideoMovieRepo {
    override fun getVideoByMovieId(movieId: Int): Maybe<VideoListModel> {
        return videoMovieLocalDataSource.getVideoByMovieIdLocal(movieId)
    }

    override fun refreshVideoByMovie(listMovies: List<MovieResult>): Completable {
        return Completable.concat(listMovies.map {
            videoMovieNetworkDataSource.getVideoByMovieId(it.id).doOnSuccess {
                Log.i("video", "get video tag $it")
                videoMovieLocalDataSource.saveVideoByMovie(it.id, it).observeOn(
                    Schedulers.computation()
                )
                    .subscribe()
            }.doOnError {
                Log.i("MovieRepoImpl", "No devuelve video por $it.id")
            }.toCompletable()
        })
    }

    override fun getVideoByMovieOnline(movieId: Int): Single<VideoListModel> {
        return videoMovieNetworkDataSource.getVideoByMovieId(movieId).map {
            it.toMovieListModel2()
        }
    }
}