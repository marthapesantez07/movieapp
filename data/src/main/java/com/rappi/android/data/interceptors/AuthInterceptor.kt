package com.rappi.android.data.interceptors

import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.TOKEN
import com.rappi.android.data.configuration.SharedPreferencesManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor : Interceptor {
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    companion object {
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // If token has been saved, add it to the request
        requestBuilder.addHeader(AUTHORIZATION, "$BEARER $TOKEN")
        /*  sharedPreferencesManager.fetchAuthToken()?.let {
              requestBuilder.addHeader(AUTHORIZATION, "$BEARER $it")
          }
         */

        return chain.proceed(requestBuilder.build())
    }
}