package com.rappi.android.data.configuration

import android.app.Application
import android.content.Context
import javax.inject.Inject

class SharedPreferencesManager @Inject constructor(val application: Application) {

    companion object {
        const val USER_TOKEN = "user_token"
    }

    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String) {
        val preferences =
            application.getSharedPreferences(USER_TOKEN, Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(USER_TOKEN, token)
        editor.apply()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? {
        val preferences =
            application.getSharedPreferences(USER_TOKEN, Context.MODE_PRIVATE)
        return preferences.getString(USER_TOKEN, null)
    }
}