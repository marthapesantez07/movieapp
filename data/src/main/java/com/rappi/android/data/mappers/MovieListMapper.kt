package com.rappi.android.data.mappers

import com.rappi.android.data.db.entities.MovieListEntity
import com.rappi.android.data.db.entities.MovieListWithMovieResultWrapper
import com.rappi.android.data.db.entities.MovieResultEntity
import com.rappi.android.data.db.entities.MovieResultWithVideoWrapper
import com.rappi.android.domain.enums.MovieCategory
import com.rappi.android.domain.models.MovieListModel
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.networkObjects.MovieList
import com.rappi.android.domain.networkObjects.MovieResult

fun MovieList.toMovieListEntityPaging(
    category: String,
    nextKey: Int?,
    prevKey: Int?
): MovieListEntity {
    return MovieListEntity(
        page = this.page,
        totalResults = this.totalResults,
        totalPages = this.totalPages,
        category = MovieCategory.getCategory(category),
        nextKey = nextKey,
        prevKey = prevKey
    )
}

fun MovieResult.toMovieResultEntity(idMovieList: Long): MovieResultEntity {
    return MovieResultEntity(
        idMovie = this.id,
        idMovieList = idMovieList,
        posterPath = this.posterPath,
        adult = this.adult,
        overview = this.overview,
        releaseDate = this.releaseDate,
        originalTitle = this.originalTitle,
        originalLanguage = this.originalLanguage,
        title = this.title,
        backdropPath = this.backdropPath,
        popularity = this.popularity,
        voteCount = this.voteCount,
        video = this.video,
        voteAverage = this.voteAverage
    )
}

fun List<MovieResult>.toListMovieResultEntity(idMovieList: Long): List<MovieResultEntity> {
    val movieResult = mutableListOf<MovieResultEntity>()
    this.forEach {
        movieResult.add(
            MovieResultEntity(
                idMovie = it.id,
                idMovieList = idMovieList,
                posterPath = it.posterPath,
                adult = it.adult,
                overview = it.overview,
                releaseDate = it.releaseDate,
                originalTitle = it.originalTitle,
                originalLanguage = it.originalLanguage,
                title = it.title,
                backdropPath = it.backdropPath,
                popularity = it.popularity,
                voteCount = it.voteCount,
                video = it.video,
                voteAverage = it.voteAverage
            )
        )
    }
    return movieResult
}

fun MovieListWithMovieResultWrapper.toMovieListDomain(): MovieListModel {
    val resultsMovie = mutableListOf<MovieResultModel>()
    val movieList = this.movieListEntity.idMovieList
    this.results.forEach {
        resultsMovie.add(it.toMovieResultDomain(movieList))
    }
    return MovieListModel(
        movieList = this.movieListEntity.idMovieList,
        page = this.movieListEntity.page,
        results = resultsMovie,
        totalPages = this.movieListEntity.totalPages,
        totalResults = this.movieListEntity.totalResults,
        nextKey = this.movieListEntity.nextKey,
        prevKey = this.movieListEntity.prevKey
    )

}

fun MovieListEntity.toMovieListDomain(): MovieListModel {
    return MovieListModel(
        movieList = this.idMovieList,
        page = this.page,
        results = mutableListOf(),
        totalPages = this.totalPages,
        totalResults = this.totalResults,
        nextKey = this.nextKey,
        prevKey = this.prevKey
    )
}

fun MovieResultWithVideoWrapper.toMovieResultDomain(movieList: Long): MovieResultModel {
    return MovieResultModel(
        idMovieList = movieList,
        posterPath = this.movieResultEntity.posterPath,
        adult = this.movieResultEntity.adult,
        overview = this.movieResultEntity.overview,
        releaseDate = this.movieResultEntity.releaseDate,
        id = this.movieResultEntity.idMovie,
        originalTitle = this.movieResultEntity.originalTitle,
        originalLanguage = this.movieResultEntity.originalLanguage,
        title = this.movieResultEntity.title,
        backdropPath = this.movieResultEntity.backdropPath,
        popularity = this.movieResultEntity.popularity,
        voteCount = this.movieResultEntity.voteCount,
        video = this.movieResultEntity.video,
        voteAverage = this.movieResultEntity.voteAverage
    )
}


fun MovieResultEntity.toModelResult(): MovieResultModel {
    return MovieResultModel(
        idMovieList = this.idMovieList,
        posterPath = this.posterPath,
        adult = this.adult,
        overview = this.overview,
        releaseDate = this.releaseDate,
        id = this.idMovie,
        originalTitle = this.originalTitle,
        originalLanguage = this.originalLanguage,
        title = this.title,
        backdropPath = this.backdropPath,
        popularity = this.popularity,
        voteCount = this.voteCount,
        video = this.video,
        voteAverage = this.voteAverage
    )
}

fun MovieResult.toMovieListResultModel(): MovieResultModel {
    return MovieResultModel(
        idMovieList = 0,
        posterPath = this.posterPath,
        adult = this.adult,
        overview = this.overview,
        releaseDate = this.releaseDate,
        id = this.id,
        originalTitle = this.originalTitle,
        originalLanguage = this.originalLanguage,
        title = this.title,
        backdropPath = this.backdropPath,
        popularity = this.popularity,
        voteCount = this.voteCount,
        video = this.video,
        voteAverage = this.voteAverage
    )
}