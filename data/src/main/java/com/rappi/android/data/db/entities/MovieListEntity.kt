package com.rappi.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rappi.android.domain.enums.MovieCategory

@Entity(tableName = MovieListEntity.TABLE_NAME)
data class MovieListEntity(
    @PrimaryKey(autoGenerate = true)
    var idMovieList: Long = 0,
    var page: Int = -1,
    var totalResults: Int = 0,
    var totalPages: Int = 0,
    var category: MovieCategory = MovieCategory.UNKNOWN,
    var prevKey: Int? = null,
    var nextKey: Int? = null
) {
    companion object {
        const val TABLE_NAME = "MovieListEntity"
    }
}