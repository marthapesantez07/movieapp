package com.rappi.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = VideoEntity.TABLE_NAME,
)
data class VideoEntity(
    @PrimaryKey
    var idVideo: String = "",
    var idMovie: Int = -1,
    var iso6391: String = "",
    var iso31661: String = "",
    var name: String = "",
    var key: String = "",
    var site: String = "",
    var size: Int = -1,
    var type: String = "",
    var official: Boolean = false,
    var publishedAt: String = "",
) {
    companion object {
        const val TABLE_NAME = "VideoEntity"
    }
}