package com.rappi.android.data.apiService.constants

class ApiServiceConstants {
    companion object {
        const val URL_API_SERVICE = "https://api.themoviedb.org/3/"
        const val GET_POPULAR_MOVIES = "movie/popular"
        const val GET_TOP_RATE_MOVIES = "movie/top_rated"
        const val GET_SEARCH_MOVIE = "search/movie"
        const val GET_VIDEO_MOVIE = "movie/{movie_id}/videos"
        const val GET_IMAGE_MOVIE = "http://image.tmdb.org/t/p/w185"
        const val SHOW_VIDEO_MOVIE = "https://www.youtube.com/watch?v="
        const val API_KEY = "AIzaSyCdH2mvSUQGVml4UYvn7ct9f0YFLW-iO_Y"
        const val TOKEN =
            "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ZDNlMjk3NWMwN2M0ZTkzNDk4YTY5MDA3ODE4OWM0MyIsInN1YiI6IjYxZGE1NTFkZjhhZWU4MDA2YjQzYTA4MCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.hKX2pp60haLOwt3GJXMj3L5kQLFlTu2wr269CVynh9k"
    }
}