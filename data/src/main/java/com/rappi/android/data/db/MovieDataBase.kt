package com.rappi.android.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rappi.android.data.db.converts.ConvertDb
import com.rappi.android.data.db.dao.MovieResultDAO
import com.rappi.android.data.db.dao.MovieWithResultWrapperDAO
import com.rappi.android.data.db.dao.VideoMovieDAO
import com.rappi.android.data.db.entities.MovieListEntity
import com.rappi.android.data.db.entities.MovieResultEntity
import com.rappi.android.data.db.entities.VideoEntity

@Database(
    entities = [MovieListEntity::class, MovieResultEntity::class, VideoEntity::class
    ],
    version = MovieDataBase.VERSION, exportSchema = false
)
@TypeConverters(ConvertDb::class)
abstract class MovieDataBase : RoomDatabase() {
    abstract fun movieWithResultWrapperDAO(): MovieWithResultWrapperDAO
    abstract fun movieResultDAO(): MovieResultDAO
    abstract fun videoMovieDAO(): VideoMovieDAO

    companion object {
        const val VERSION = 1
    }
}