package com.rappi.android.data.di

import com.rappi.android.data.apiService.ApiService
import com.rappi.android.data.datasourceImpl.local.MovieLocalDatasourceImpl
import com.rappi.android.data.datasourceImpl.local.VideoMovieLocalDatasourceImpl
import com.rappi.android.data.datasourceImpl.remote.MovieNetworkDatasourceImpl
import com.rappi.android.data.datasourceImpl.remote.VideoMovieNetworkDatasourceImpl
import com.rappi.android.data.db.dao.MovieWithResultWrapperDAO
import com.rappi.android.data.db.dao.VideoMovieDAO
import com.rappi.android.domain.datasource.local.MovieLocalDataSource
import com.rappi.android.domain.datasource.local.VideoMovieLocalDataSource
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.datasource.remote.VideoMovieNetworkDataSource
import dagger.Module
import dagger.Provides

@Module
class DataSourceModule {

    @Provides
    fun provideMovieNetworkDataSource(apiService: ApiService): MovieNetworkDatasource {
        return MovieNetworkDatasourceImpl(apiService)
    }

    @Provides
    fun provideMovieLocalDataSource(movieWithResultWrapperDAO: MovieWithResultWrapperDAO): MovieLocalDataSource {
        return MovieLocalDatasourceImpl(movieWithResultWrapperDAO)
    }

    @Provides
    fun provideVideoMovieNetworkDataSource(apiService: ApiService): VideoMovieNetworkDataSource {
        return VideoMovieNetworkDatasourceImpl(apiService)
    }

    @Provides
    fun provideVideoMovieLocalDataSource(videoMovieDAO: VideoMovieDAO): VideoMovieLocalDataSource {
        return VideoMovieLocalDatasourceImpl(videoMovieDAO)
    }
}