package com.rappi.android.data.datasourceImpl.remote

import com.rappi.android.data.apiService.ApiService
import com.rappi.android.domain.datasource.remote.VideoMovieNetworkDataSource
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Single
import javax.inject.Inject

class VideoMovieNetworkDatasourceImpl @Inject constructor(private val apiService: ApiService) :
    VideoMovieNetworkDataSource {
    override fun getVideoByMovieId(movieId: Int): Single<VideoList> {
        return apiService.getVideoMovie(movieId)
    }
}