package com.rappi.android.data.di

import android.app.Application
import androidx.room.Room
import com.rappi.android.data.db.MovieDataBase
import com.rappi.android.data.db.dao.MovieResultDAO
import com.rappi.android.data.db.dao.MovieWithResultWrapperDAO
import com.rappi.android.data.db.dao.VideoMovieDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    companion object {
        const val MOVIE_DB_NAME = "MOVIE_DB"
    }

    @Singleton
    @Provides
    fun provideGeneralApplicationDataBase(application: Application): MovieDataBase {
        return Room.databaseBuilder(
            application.applicationContext, MovieDataBase::class.java,
            MOVIE_DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideMovieWithResultWrapperDao(movieDataBase: MovieDataBase): MovieWithResultWrapperDAO {
        return movieDataBase.movieWithResultWrapperDAO()
    }

    @Singleton
    @Provides
    fun provideMovieResultDao(movieDataBase: MovieDataBase): MovieResultDAO {
        return movieDataBase.movieResultDAO()
    }

    @Singleton
    @Provides
    fun provideVideoMovieDao(movieDataBase: MovieDataBase): VideoMovieDAO {
        return movieDataBase.videoMovieDAO()
    }
}