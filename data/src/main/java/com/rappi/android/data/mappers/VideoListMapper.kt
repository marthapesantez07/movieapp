package com.rappi.android.data.mappers

import com.rappi.android.data.db.entities.VideoEntity
import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.models.VideoResultModel
import com.rappi.android.domain.networkObjects.VideoList
import com.rappi.android.domain.networkObjects.VideoResult

fun List<VideoEntity>.toVideoListModel(movieId: Int): VideoListModel {
    val videoResultModel = mutableListOf<VideoResultModel>()
    this.forEach { it ->
        videoResultModel.add(
            VideoResultModel(
                iso6391 = it.iso6391,
                iso31661 = it.iso31661,
                name = it.name,
                key = it.key,
                site = it.site,
                size = it.size.toString(),
                type = it.type,
                official = it.official,
                publishedAt = it.publishedAt,
                id = it.idVideo
            )
        )
    }
    return VideoListModel(movieId, videoResultModel)
}

fun List<VideoResult>.toListVideoEntity(movieId: Int): List<VideoEntity> {
    val listVideoEntity = mutableListOf<VideoEntity>()
    this.forEach { it ->
        listVideoEntity.add(
            VideoEntity(
                idVideo = it.id,
                idMovie = movieId,
                iso31661 = it.iso31661,
                iso6391 = it.iso6391,
                name = it.name,
                key = it.key,
                site = it.site,
                size = it.size.toInt(),
                type = it.type,
                official = it.official,
                publishedAt = it.publishedAt,
            )
        )
    }
    return listVideoEntity
}

fun VideoList.toMovieListModel2(): VideoListModel {
    val videoResultModel = mutableListOf<VideoResultModel>()
    this.results.forEach { it ->
        videoResultModel.add(
            VideoResultModel(
                iso6391 = it.iso6391,
                iso31661 = it.iso31661,
                name = it.name,
                key = it.key,
                site = it.site,
                size = it.size,
                type = it.type,
                official = it.official,
                publishedAt = it.publishedAt,
                id = it.id
            )
        )
    }
    return VideoListModel(this.id, videoResultModel)
}