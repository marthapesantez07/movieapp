package com.rappi.android.data.datasourceImpl

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.rappi.android.data.datasourceImpl.Constants.Companion.DEFAULT_PAGE_INDEX
import com.rappi.android.data.db.entities.MovieResultEntity
import com.rappi.android.domain.datasource.local.MovieLocalDataSource
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.enums.MovieCategory
import com.rappi.android.domain.models.MovieListModel
import com.rappi.android.domain.networkObjects.MovieList
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@ExperimentalPagingApi
class MovieMediator @Inject constructor(
    private val movieLocalDataSource: MovieLocalDataSource,
    private val movieNetworkDatasource: MovieNetworkDatasource,
    private val category: String
) : RemoteMediator<Int, MovieResultEntity>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, MovieResultEntity>
    ): MediatorResult {
        val pageKeyData = getKeyPageData(loadType, state)
        val page = when (pageKeyData) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }
        try {
            val apiResponse = getMovieNetwork(page)
            val repos = apiResponse?.results
            val endOfPaginationReached = repos?.isEmpty() ?: false
            apiResponse?.let {
                movieLocalDataSource.saveMoviePaging(
                    category,
                    it,
                    loadType,
                    page.toString().toInt(),
                    endOfPaginationReached
                )
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getMovieNetwork(page: Int): MovieList? {
        return when (category) {
            MovieCategory.POPULAR.name -> movieNetworkDatasource.getPopularMoviePaging(page)
            MovieCategory.TOP_RATED.name ->
                movieNetworkDatasource.getTopRatePaging(page)
            else -> null
        }


    }

    //get the last key the last item
    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, MovieResultEntity>): MovieListModel? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                movieLocalDataSource.getRemoteKeys(repo.idMovieList.toInt())
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, MovieResultEntity>): MovieListModel? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                movieLocalDataSource.getRemoteKeys(repo.idMovieList.toInt())
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, MovieResultEntity>
    ): MovieListModel? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.idMovieList?.let { repoId ->
                movieLocalDataSource.getRemoteKeys(repoId.toInt())
            }
        }
    }

    /**
     * this returns the page key or the final end of list success result
     */
    private suspend fun getKeyPageData(
        loadType: LoadType,
        state: PagingState<Int, MovieResultEntity>
    ): Any? {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                // If remoteKeys is null, that means the refresh result is not in the database yet.
                // We can return Success with `endOfPaginationReached = false` because Paging
                // will call this method again if RemoteKeys becomes non-null.
                // If remoteKeys is NOT NULL but its prevKey is null, that means we've reached
                // the end of pagination for prepend.
                //  ?: throw InvalidObjectException("Invalid state, key should not be null")
                //end of list condition reached
                val prevKey = remoteKeys?.prevKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                // If remoteKeys is null, that means the refresh result is not in the database yet.
                // We can return Success with endOfPaginationReached = false because Paging
                // will call this method again if RemoteKeys becomes non-null.
                // If remoteKeys is NOT NULL but its prevKey is null, that means we've reached
                // the end of pagination for append.
                val nextKey = remoteKeys?.nextKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                nextKey
            }
        }
    }

}