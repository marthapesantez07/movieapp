package com.rappi.android.data.datasourceImpl

class Constants {
    companion object {
        const val DEFAULT_PAGE_INDEX = 1
        const val DEFAULT_PAGE_SIZE = 20
    }
}