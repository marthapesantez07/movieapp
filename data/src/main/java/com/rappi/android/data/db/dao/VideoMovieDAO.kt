package com.rappi.android.data.db.dao

import androidx.room.*
import com.rappi.android.data.db.entities.VideoEntity
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface VideoMovieDAO {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertVideoResult(videoEntity: List<VideoEntity>): Completable

    @Query("SELECT * FROM ${VideoEntity.TABLE_NAME} WHERE idMovie=:movieId")
    fun getVideoByMovie(movieId: Int): Maybe<List<VideoEntity>>

    @Query("DELETE FROM ${VideoEntity.TABLE_NAME} WHERE idMovie=:movieId")
    fun deleteVideoByMovie(movieId: Int): Completable
}