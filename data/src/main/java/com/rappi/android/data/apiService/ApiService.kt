package com.rappi.android.data.apiService

import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_POPULAR_MOVIES
import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_SEARCH_MOVIE
import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_TOP_RATE_MOVIES
import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_VIDEO_MOVIE
import com.rappi.android.domain.networkObjects.MovieList
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET(GET_POPULAR_MOVIES)
    suspend fun getPopularMoviePaging(
        @Query("page") page: Int
    ): MovieList?

    @GET(GET_TOP_RATE_MOVIES)
    suspend fun getTopRateMoviePaging(
        @Query("page") page: Int
    ): MovieList?

    @GET(GET_SEARCH_MOVIE)
    suspend fun getSearchMoviePaging(
        @Query("query") query: String,
        @Query("page") page: Int
    ): MovieList?

    @GET(GET_VIDEO_MOVIE)
    fun getVideoMovie(@Path("movie_id") movieId: Int): Single<VideoList>
}