package com.rappi.android.data.di

import android.app.Application
import com.rappi.android.data.configuration.SharedPreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {
    @Provides
    @Singleton
    fun sharedPreferencesManager(application: Application): SharedPreferencesManager = SharedPreferencesManager(application)
}