package com.rappi.android.data.extensionFuntions

import android.util.Log
import retrofit2.Response
import java.io.IOException

/**
 * Try to call a web service and return a sealed class [Result]
 * when call is success it returns [Result.Success] or [Result.Error] if failed
 */
suspend fun <T : Any> safeApiCallApiRest(
    call: suspend () -> Result<T>
): Result<T> = try {
    call.invoke()
} catch (e: Exception) {
    Log.e("safeApiCallRest", e.message.toString())
    e.printStackTrace()
    Result.Error(IOException(e))
}

/**
 * When Response in method isSuccess is true and property body is not null return [Result.Success]
 * or [Result.Error] in any other condition
 */
fun <T : Any> Response<T>.callServicesApiRest(): Result<T> {
    val errorMessage = this.message()
    if (this.isSuccessful) {
        return if (this.body() != null) {
            Result.Success(this.body() as T)
        } else {
            Result.Error(NullPointerException(errorMessage))
        }
    }
    return Result.Error(Throwable(errorMessage))
}

suspend fun <T : Any> safeCallLocalDb(
    call: suspend () -> Result<T>
): Result<T> = try {
    call.invoke()
} catch (e: Exception) {
    Log.e("safeApiCallRest", e.message.toString())
    e.printStackTrace()
    Result.Error(IOException(e))
}


fun <T : Any> Response<T>.callLocalDb(): Result<T> {
    val errorMessage = this.message()
    if (this.isSuccessful) {
        return if (this.body() != null) {
            Result.Success(this.body() as T)
        } else {
            Result.Error(NullPointerException(errorMessage))
        }
    }
    return Result.Error(Throwable(errorMessage))
}