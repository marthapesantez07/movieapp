package com.rappi.android.data.di

import com.rappi.android.data.db.dao.MovieResultDAO
import com.rappi.android.data.repositoryImpl.MovieRepoImpl
import com.rappi.android.data.repositoryImpl.VideoMovieRepoImpl
import com.rappi.android.domain.datasource.local.MovieLocalDataSource
import com.rappi.android.domain.datasource.local.VideoMovieLocalDataSource
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.datasource.remote.VideoMovieNetworkDataSource
import com.rappi.android.domain.repository.MovieRepo
import com.rappi.android.domain.repository.VideoMovieRepo
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun provideRemoteRepo(
        movieNetworkDatasource: MovieNetworkDatasource,
        movieLocalDataSource: MovieLocalDataSource,
        movieResultDAO: MovieResultDAO
    ): MovieRepo {
        return MovieRepoImpl(
            movieNetworkDatasource,
            movieLocalDataSource,
            movieResultDAO
        )
    }

    @Provides
    fun provideVideoMovieRepo(
        videoMovieLocalDataSource: VideoMovieLocalDataSource,
        videoMovieNetworkDataSource: VideoMovieNetworkDataSource
    ): VideoMovieRepo {
        return VideoMovieRepoImpl(videoMovieLocalDataSource, videoMovieNetworkDataSource)
    }
}