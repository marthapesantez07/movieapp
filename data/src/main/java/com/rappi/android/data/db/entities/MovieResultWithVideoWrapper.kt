package com.rappi.android.data.db.entities

import androidx.room.Embedded
import androidx.room.Relation

data class MovieResultWithVideoWrapper(
    @Embedded
    var movieResultEntity: MovieResultEntity = MovieResultEntity(),
    @Relation(
        entity = VideoEntity::class,
        parentColumn = "idMovie",
        entityColumn = "idMovie"
    )
    var results: List<VideoEntity>? = listOf()
)