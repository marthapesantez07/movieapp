package com.rappi.android.data.datasourceImpl

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.rappi.android.data.datasourceImpl.Constants.Companion.DEFAULT_PAGE_INDEX
import com.rappi.android.domain.datasource.remote.MovieNetworkDatasource
import com.rappi.android.domain.networkObjects.MovieResult
import retrofit2.HttpException
import java.io.IOException

class SearchMoviePagingSource(
    private val movieNetworkDatasource: MovieNetworkDatasource,
    private val query: String
) : PagingSource<Int, MovieResult>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieResult> {
        //for first case it will be null, then we can pass some default value, in our case it's 1
        val page = params.key ?: DEFAULT_PAGE_INDEX
        return try {
            val response = movieNetworkDatasource.getSearchMoviePaging(query, page)
            LoadResult.Page(
                response?.results ?: listOf(),
                prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1,
                nextKey = if (response?.results?.isEmpty() == true) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, MovieResult>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.minus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.plus(1)
        }
    }
}