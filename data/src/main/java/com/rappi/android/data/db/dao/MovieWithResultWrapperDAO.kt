package com.rappi.android.data.db.dao

import androidx.paging.LoadType
import androidx.room.*
import com.rappi.android.data.datasourceImpl.Constants
import com.rappi.android.data.db.entities.MovieListEntity
import com.rappi.android.data.db.entities.MovieResultEntity
import com.rappi.android.data.db.entities.MovieResultWithVideoWrapper
import com.rappi.android.data.mappers.toListMovieResultEntity
import com.rappi.android.data.mappers.toMovieListEntityPaging
import com.rappi.android.domain.networkObjects.MovieList

@Dao
abstract class MovieWithResultWrapperDAO {

    //paging////////////////////
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insertMovieListPaging(movieListEntity: MovieListEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insertMovieResultPaging(movieResultEntity: List<MovieResultEntity>)

    @Query("DELETE FROM ${MovieListEntity.TABLE_NAME} WHERE category=:category")
    abstract suspend fun deleteMovieListByCategoryPaging(category: String)

    @Query("DELETE FROM ${MovieResultEntity.TABLE_NAME} WHERE idMovieList in (SELECT idMovieList FROM ${MovieListEntity.TABLE_NAME} WHERE category=:category) ")
    abstract suspend fun deleteMovieResultByCategoryPaging(category: String)

    @Query("SELECT * FROM ${MovieListEntity.TABLE_NAME} WHERE idMovieList=:movieId ")
    abstract suspend fun getMovieListPaging(movieId: Int): MovieListEntity?

    @Transaction
    @Query("")
    suspend fun saveMoviePaging(
        movieCategory: String,
        movieList: MovieList,
        loadTye: LoadType,
        page: Int,
        endOfPaginationReached: Boolean
    ) {
        if (loadTye == LoadType.REFRESH) {
            deleteMovieResultByCategoryPaging(movieCategory)
            deleteMovieListByCategoryPaging(movieCategory)
        }
        val prevKey = if (page == Constants.DEFAULT_PAGE_INDEX) null else page - 1
        val nextKey = if (endOfPaginationReached) null else page + 1

        val idMovieList = insertMovieListPaging(
            movieList.toMovieListEntityPaging(movieCategory, nextKey, prevKey)
        )
        insertMovieResultPaging(
            movieList.results.toListMovieResultEntity(
                idMovieList
            )
        )
    }
    //paging////////////////////

    @Query("SELECT * FROM ${MovieResultEntity.TABLE_NAME} where title LIKE '%' || :text || '%'")
    abstract fun searchMovie(
        text: String,
    ): MovieResultWithVideoWrapper

}