package com.rappi.android.movieapp.modules.searchMovie.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.usecases.movies.GetSearchMovie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchFragmentViewModel @Inject constructor(
    private val getSearchMovie: GetSearchMovie,
) : ViewModel() {
    private var _navigateToDescriptionMLD: MutableLiveData<MovieResultModel> = MutableLiveData()
    val navigateToDescription: LiveData<MovieResultModel>
        get() = _navigateToDescriptionMLD

    fun getSearchMoviePaging(query: String): Flow<PagingData<MovieResultModel>> {
        return getSearchMovie.invokePaging(query)
            .cachedIn(viewModelScope)
    }

    fun goToDescription(movie: MovieResultModel) {
        _navigateToDescriptionMLD.value = movie
        Log.e("ListViewMovie", "navigateToDescription ->  $movie")
    }
}