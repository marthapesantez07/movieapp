package com.rappi.android.movieapp.modules.searchMovie.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.movieapp.base.obtainViewModelF
import com.rappi.android.movieapp.databinding.FragmentSearchBinding
import com.rappi.android.movieapp.modules.listMovie.view.adapter.MovieViewAdapter
import com.rappi.android.movieapp.modules.searchMovie.viewModel.SearchFragmentViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

class SearchFragment : DaggerFragment() {
    private lateinit var binding: FragmentSearchBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var searchFragmentViewModel: SearchFragmentViewModel

    private val searchMovieAdapter: MovieViewAdapter by lazy {
        MovieViewAdapter { movie ->
            searchFragmentViewModel.goToDescription(movie)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        searchFragmentViewModel = viewModelFactory.obtainViewModelF(this)
        with(binding) {
            rvSearchMovie.apply {
                adapter = searchMovieAdapter
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            }
            tietSearchMovie.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s.length > 3) {
                        searchMovieAdapter(s.toString())
                    }
                }
            })
        }
        searchFragmentViewModel.navigateToDescription.observe(viewLifecycleOwner, {
            it?.let {
                navigateToDescription(it)
            }
        })

        return binding.root
    }

    private fun searchMovieAdapter(query: String) {
        lifecycleScope.launch {
            searchFragmentViewModel.getSearchMoviePaging(query)
                .distinctUntilChanged().collectLatest {
                    searchMovieAdapter.submitData(it)
                }
        }
    }

    private fun navigateToDescription(movie: MovieResultModel) {
        findNavController().safeNavigate(
            SearchFragmentDirections.actionSearchFragmentToBottomSheetDescriptionFragment(
                movie
            )
        )
    }

    private fun NavController.safeNavigate(direction: NavDirections) {
        currentDestination?.getAction(direction.actionId)?.run {
            navigate(direction)
        }
    }
}