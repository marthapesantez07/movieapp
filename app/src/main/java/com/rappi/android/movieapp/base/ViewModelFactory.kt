package com.rappi.android.movieapp.base

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>,
            @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModels[modelClass]?.get() as T
    }


}

inline fun <reified VM : ViewModel> ViewModelProvider.Factory.obtainViewModel(activity: FragmentActivity): VM {
    return ViewModelProvider(activity, this)[VM::class.java]
}

inline fun <reified VM : ViewModel> ViewModelProvider.Factory.obtainViewModelF(fragment: DaggerFragment): VM {
    return ViewModelProvider(fragment, this)
        .get(VM::class.java)
}

inline fun <reified VM : ViewModel> ViewModelProvider.Factory.obtainViewModelBS(dialog: BottomSheetDialogFragment): VM {
    return ViewModelProvider(dialog, this).get(VM::class.java)
}

