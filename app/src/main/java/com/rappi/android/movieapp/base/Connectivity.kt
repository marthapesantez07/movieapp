package com.rappi.android.movieapp.base

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.rappi.android.movieapp.ApplicationClass
import javax.inject.Inject


interface Connectivity {
    fun isNetworkAvailable(): Boolean

    class ConnectivityImpl : Connectivity {
        @Inject
        lateinit var connectivityManager: ConnectivityManager

        init {
            ApplicationClass.appComponent.inject(this)
        }

        override fun isNetworkAvailable(): Boolean {
            connectivityManager.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    return getNetworkCapabilities(activeNetwork)?.run {
                        when {
                            hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                            hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                            hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                            hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                            else -> false
                        }
                    } ?: false
                } else {
                    return activeNetworkInfo?.isConnectedOrConnecting ?: return false
                }
            }
        }

    }
}
