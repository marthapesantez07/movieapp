package com.rappi.android.movieapp.modules.descriptionMovie.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_IMAGE_MOVIE
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.movieapp.R
import com.rappi.android.movieapp.base.obtainViewModelBS
import com.rappi.android.movieapp.cached.ConstantCached.Companion.VIDEO_LIST
import com.rappi.android.movieapp.cached.VideoPreLoadingService
import com.rappi.android.movieapp.databinding.FragmentDescriptionBinding
import com.rappi.android.movieapp.modules.descriptionMovie.viewModel.BottomSheetDescriptionViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class BottomSheetDescriptionFragment : BottomSheetDialogFragment(), HasAndroidInjector {
    private lateinit var binding: FragmentDescriptionBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var bottomSheetDescriptionViewModel: BottomSheetDescriptionViewModel

    private val args: BottomSheetDescriptionFragmentArgs by navArgs()

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private var videoList = arrayListOf<String>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDescriptionBinding.inflate(inflater, container, false)
        bottomSheetDescriptionViewModel = viewModelFactory.obtainViewModelBS(this)
        bottomSheetDescriptionViewModel.setUp(args)
        bottomSheetDescriptionViewModel.resultMovie.observe(viewLifecycleOwner, {
            it?.let {
                setValues(it)
            }
        })

        bottomSheetDescriptionViewModel.resultVideo.observe(viewLifecycleOwner, {
            it?.let {
                binding.btVer.isEnabled = true
            }
        })

        binding.btVer.setOnClickListener {
            goToVideoFragment(bottomSheetDescriptionViewModel.getVideo()?.key)
        }

        return binding.root
    }

    private fun startPreLoadingService() {
        //TODO WHEN SOLVE THE EXOPLAYER TO GET DASH URL OF YOUTUBE IS POSIBLE TO USE THIS CODE TO SHOW VIDEO IN CACHING
        val preloadingServiceIntent = Intent(context, VideoPreLoadingService::class.java)
        preloadingServiceIntent.putStringArrayListExtra(VIDEO_LIST, videoList)
        context?.startService(preloadingServiceIntent)
    }

    private fun setValues(movieResultModel: MovieResultModel) {
        with(binding) {
            txtTitle.text = movieResultModel.title
            txtDate.text = movieResultModel.releaseDate
            txtLanguage.text = movieResultModel.originalLanguage
            txtDescription.text = movieResultModel.overview
            Glide.with(this.imgThumbnail)
                .load("$GET_IMAGE_MOVIE${movieResultModel.posterPath}")
                .onlyRetrieveFromCache(true)
                .error(R.drawable.ic_movies)
                .into(imgThumbnail)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    /*---------------- Dagger Injection for Fragment -------------*/
    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun goToVideoFragment(videoLink: String?) {
        videoLink?.let {
            findNavController().navigate(
                BottomSheetDescriptionFragmentDirections.actionBottomSheetDescriptionFragmentToDetailVideoFragment(
                    videoLink
                )
            )
        }

    }


}