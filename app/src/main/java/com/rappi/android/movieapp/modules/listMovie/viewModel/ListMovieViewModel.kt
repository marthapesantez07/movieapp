package com.rappi.android.movieapp.modules.listMovie.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.usecases.movies.GetPopularMovie
import com.rappi.android.domain.usecases.movies.GetTopRateMovie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ListMovieViewModel @Inject constructor(
    private val getPopularMovie: GetPopularMovie,
    private val getTopRateMovie: GetTopRateMovie,
) : ViewModel() {
    private var _navigateToDescriptionMLD: MutableLiveData<MovieResultModel> = MutableLiveData()
    val navigateToDescription: LiveData<MovieResultModel>
        get() = _navigateToDescriptionMLD

    fun getPopularMoviePaging(): Flow<PagingData<MovieResultModel>> {
        return getPopularMovie.invokePaging()
            .cachedIn(viewModelScope)
    }

    fun getTopRatePaging(): Flow<PagingData<MovieResultModel>> {
        return getTopRateMovie.invokePaging()
            .cachedIn(viewModelScope)
    }


    fun goToDescription(movie: MovieResultModel) {
        _navigateToDescriptionMLD.value = movie
        Log.e("ListViewMovie", "navigateToDescription ->  $movie")
    }
}