package com.rappi.android.movieapp.modules.listMovie.view.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import com.rappi.android.movieapp.base.BaseHolder
import com.rappi.android.movieapp.databinding.ItemLoaderBinding
import javax.inject.Inject

class LoaderViewHolder @Inject constructor(private val binding: ItemLoaderBinding) :
    BaseHolder(binding.root) {
    fun binding(retry: () -> Unit, loadState: LoadState) {
        if (loadState is LoadState.Error) {
            binding.tvError.text = loadState.error.localizedMessage
        }
        binding.pbLoader.isVisible = loadState is LoadState.Loading
        binding.tvError.isVisible = loadState !is LoadState.Loading
        binding.btnRetry.isVisible = loadState !is LoadState.Loading
        if (loadState is LoadState.Loading)
            binding.motion.transitionToEnd()
        else
            binding.motion.transitionToStart()
        binding.btnRetry.setOnClickListener {
            retry()
        }
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): LoaderViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return LoaderViewHolder(ItemLoaderBinding.inflate(inflater, parent, false))
        }
    }
}