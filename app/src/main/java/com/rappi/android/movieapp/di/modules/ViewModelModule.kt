package com.rappi.android.movieapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rappi.android.movieapp.annotation.ViewModelKey
import com.rappi.android.movieapp.base.ViewModelFactory
import com.rappi.android.movieapp.modules.descriptionMovie.viewModel.BottomSheetDescriptionViewModel
import com.rappi.android.movieapp.modules.detailVideo.viewModel.DetailVideoViewModel
import com.rappi.android.movieapp.modules.listMovie.viewModel.ListMovieViewModel
import com.rappi.android.movieapp.modules.main.viewModel.MainViewModel
import com.rappi.android.movieapp.modules.searchMovie.viewModel.SearchFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds
    @Singleton
    abstract fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun provideMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListMovieViewModel::class)
    abstract fun provideListMovieViewModel(listMovieViewModel: ListMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailVideoViewModel::class)
    abstract fun provideDetailVideoViewModel(detailVideoViewModel: DetailVideoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BottomSheetDescriptionViewModel::class)
    abstract fun provideBottomSheetDescription(bottomSheetDescriptionViewModel: BottomSheetDescriptionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchFragmentViewModel::class)
    abstract fun provideSearchViewModel(searchFragmentViewModel: SearchFragmentViewModel): ViewModel

}