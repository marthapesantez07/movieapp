package com.rappi.android.movieapp.modules.detailVideo.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rappi.android.movieapp.modules.detailVideo.view.DetailVideoFragmentArgs
import javax.inject.Inject

class DetailVideoViewModel @Inject constructor() : ViewModel() {
    private val _linkVideo: MutableLiveData<String> = MutableLiveData()
    val linkVideo: LiveData<String>
        get() = _linkVideo

    fun setUp(args: DetailVideoFragmentArgs) {
        _linkVideo.value = args.linkVideo
    }
}