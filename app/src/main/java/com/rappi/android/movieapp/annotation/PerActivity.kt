package com.rappi.android.movieapp.annotation

import javax.inject.Scope

@Scope
internal annotation class PerActivity