package com.rappi.android.movieapp.modules.listMovie.view.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.rappi.android.data.apiService.constants.ApiServiceConstants.Companion.GET_IMAGE_MOVIE
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.movieapp.R
import com.rappi.android.movieapp.base.BaseHolder
import com.rappi.android.movieapp.databinding.ItemPosterMovieBinding
import javax.inject.Inject

class MovieViewHolder @Inject constructor(private val binding: ItemPosterMovieBinding) :
    BaseHolder(binding.root) {

    fun binding(
        item: MovieResultModel,
        onClickToDescription: (MovieResultModel) -> Unit,
    ) {
        setPhoto("$GET_IMAGE_MOVIE${item.posterPath}")
        binding.posterMovie.setOnClickListener { onClickToDescription(item) }
    }

    private fun setPhoto(url: String?) {
        Glide.with(this.itemView)
            .load(url)
            .transition(GenericTransitionOptions.with(R.anim.zoom_in))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .error(R.drawable.ic_movies)
            .into(binding.posterMovie)
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): MovieViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return MovieViewHolder(ItemPosterMovieBinding.inflate(inflater, parent, false))
        }
    }
}