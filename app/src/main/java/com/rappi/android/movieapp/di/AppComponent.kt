package com.rappi.android.movieapp.di

import android.app.Application
import com.rappi.android.data.di.*
import com.rappi.android.movieapp.ApplicationClass
import com.rappi.android.movieapp.base.Connectivity
import com.rappi.android.movieapp.di.modules.ActivityBuilderModule
import com.rappi.android.movieapp.di.modules.AndroidModule
import com.rappi.android.movieapp.di.modules.RxModule
import com.rappi.android.movieapp.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        AndroidModule::class,
        RxModule::class,
        RoomModule::class,
        DataSourceModule::class,
        RepositoryModule::class,
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {
    fun inject(app: ApplicationClass)
    fun inject(connectivity: Connectivity.ConnectivityImpl)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}