package com.rappi.android.movieapp.modules.listMovie.view.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.movieapp.modules.listMovie.view.holder.MovieViewHolder
import javax.inject.Inject

class MovieViewAdapter @Inject constructor(
    private val onClickToDescription: (
        MovieResultModel
    ) -> Unit
) : PagingDataAdapter<MovieResultModel, MovieViewHolder>(MovieDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder.createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { holder.binding(it, onClickToDescription) }
    }

}

class MovieDiffCallback : DiffUtil.ItemCallback<MovieResultModel>() {
    override fun areItemsTheSame(
        oldItem: MovieResultModel,
        newItem: MovieResultModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: MovieResultModel,
        newItem: MovieResultModel
    ): Boolean {
        return oldItem == newItem
    }
}