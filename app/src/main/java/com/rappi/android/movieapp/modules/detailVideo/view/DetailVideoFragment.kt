package com.rappi.android.movieapp.modules.detailVideo.view

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.youtube.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.loadOrCueVideo
import com.rappi.android.movieapp.ApplicationClass
import com.rappi.android.movieapp.base.obtainViewModelF
import com.rappi.android.movieapp.databinding.FragmentDetailVideoBinding
import com.rappi.android.movieapp.modules.detailVideo.viewModel.DetailVideoViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class DetailVideoFragment : DaggerFragment() {
    private lateinit var binding: FragmentDetailVideoBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var detailVideoViewModel: DetailVideoViewModel

    private val args: DetailVideoFragmentArgs by navArgs()

    //ExoPlayer
    private lateinit var httpDataSourceFactory: HttpDataSource.Factory
    private lateinit var defaultDataSourceFactory: DefaultDataSourceFactory
    private lateinit var cacheDataSourceFactory: DataSource.Factory
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private val simpleCache: Cache = ApplicationClass.simpleCache

    //YOUTUBE API
    private var mYouTubePlayer: YouTubePlayer? = null
    private var mApiKey: String? = null
    private lateinit var mVideoId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailVideoBinding.inflate(inflater, container, false)
        detailVideoViewModel = viewModelFactory.obtainViewModelF(this)
        detailVideoViewModel.setUp(args)

        detailVideoViewModel.linkVideo.observe(viewLifecycleOwner, {
            it?.let {
                mVideoId = it
            }
        })
        lifecycle.addObserver(binding.youtubeVideo);
        binding.youtubeVideo.getPlayerUiController().showFullscreenButton(true)
        binding.youtubeVideo.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                val videoId = args.linkVideo
                Log.i("video", "video a mostrar es $videoId")
                youTubePlayer.loadOrCueVideo(lifecycle, videoId, 0f)
            }
        })
        return binding.root
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.youtubeVideo.enterFullScreen()
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            binding.youtubeVideo.exitFullScreen()
        }
    }

    private fun initPlayer(videoUrl: String) {
        //TODO WHEN SOLVE THE EXOPLAYER TO GET DASH URL OF YOUTUBE IS POSIBLE TO USE THIS CODE TO SHOW VIDEO IN CACHING
        httpDataSourceFactory = DefaultHttpDataSource.Factory()
            .setAllowCrossProtocolRedirects(true)

        defaultDataSourceFactory = DefaultDataSourceFactory(
            requireContext(), httpDataSourceFactory
        )
        cacheDataSourceFactory = CacheDataSource.Factory()
            .setCache(simpleCache)
            .setUpstreamDataSourceFactory(httpDataSourceFactory)
            .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)



        simpleExoPlayer = SimpleExoPlayer.Builder(requireContext())
            .setMediaSourceFactory(DefaultMediaSourceFactory(cacheDataSourceFactory)).build()

        val videoUri = Uri.parse(videoUrl)
        val mediaItem = MediaItem.fromUri(videoUri)
        val mediaSource =
            ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem)

        /* binding.playerView.player = simpleExoPlayer
         simpleExoPlayer.playWhenReady = true
         simpleExoPlayer.seekTo(0, 0)
         simpleExoPlayer.repeatMode = Player.REPEAT_MODE_OFF
         simpleExoPlayer.setMediaSource(mediaSource, true)
         simpleExoPlayer.prepare()
 */
    }
}