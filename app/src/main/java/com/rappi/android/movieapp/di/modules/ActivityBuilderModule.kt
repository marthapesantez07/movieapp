package com.rappi.android.movieapp.di.modules

import com.rappi.android.movieapp.annotation.PerActivity
import com.rappi.android.movieapp.modules.descriptionMovie.view.BottomSheetDescriptionFragment
import com.rappi.android.movieapp.modules.detailVideo.view.DetailVideoFragment
import com.rappi.android.movieapp.modules.listMovie.view.ListMovieFragment
import com.rappi.android.movieapp.modules.main.view.MainActivity
import com.rappi.android.movieapp.modules.searchMovie.view.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideListMovieFragment(): ListMovieFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideDetailVideo(): DetailVideoFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideBottomSheetDescription(): BottomSheetDescriptionFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideSearchFragment(): SearchFragment

}