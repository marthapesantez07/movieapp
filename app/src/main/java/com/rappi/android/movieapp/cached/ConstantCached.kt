package com.rappi.android.movieapp.cached

class ConstantCached {
    companion object {
        const val VIDEO_LIST = "VIDEO_LIST"
        const val VIDEO_URL = "VIDEO_URL"
    }
}