package com.rappi.android.movieapp.modules.listMovie.view

import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.movieapp.R
import com.rappi.android.movieapp.base.obtainViewModelF
import com.rappi.android.movieapp.databinding.FragmentListMovieBinding
import com.rappi.android.movieapp.modules.listMovie.view.adapter.LoaderViewAdapter
import com.rappi.android.movieapp.modules.listMovie.view.adapter.MovieViewAdapter
import com.rappi.android.movieapp.modules.listMovie.viewModel.ListMovieViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListMovieFragment : DaggerFragment() {
    private lateinit var binding: FragmentListMovieBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var listViewModel: ListMovieViewModel

    private val popularMovieAdapter: MovieViewAdapter by lazy {
        MovieViewAdapter { movie ->
            listViewModel.goToDescription(movie)
        }
    }
    private val loaderPopularMovie: LoaderViewAdapter by lazy {
        LoaderViewAdapter {
            popularMovieAdapter.retry()
        }
    }
    private val topMovieAdapter: MovieViewAdapter by lazy {
        MovieViewAdapter { movie ->
            listViewModel.goToDescription(movie)
        }
    }

    private val loaderTopRated: LoaderViewAdapter by lazy {
        LoaderViewAdapter {
            topMovieAdapter.retry()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListMovieBinding.inflate(inflater, container, false)
        listViewModel = viewModelFactory.obtainViewModelF(this)
        getPopularMovie()
        getTopMovie()
        with(binding) {
            rvListMoviePopular.apply {
                adapter = popularMovieAdapter.withLoadStateFooter(loaderPopularMovie)
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

            }

        }

        with(binding) {
            rvListMovieTopRated.apply {
                adapter = topMovieAdapter.withLoadStateFooter(loaderTopRated)
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            }

        }

        listViewModel.navigateToDescription.observe(viewLifecycleOwner, {
            it?.let {
                navigateToDescription(it)
            }
        })
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun getPopularMovie() {
        lifecycleScope.launch {
            listViewModel.getPopularMoviePaging().distinctUntilChanged().collectLatest {
                popularMovieAdapter.submitData(it)
            }
        }
    }

    private fun getTopMovie() {
        lifecycleScope.launch {
            listViewModel.getTopRatePaging().distinctUntilChanged().collectLatest {
                topMovieAdapter.submitData(it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

    private fun navigateToDescription(movie: MovieResultModel) {
        findNavController().safeNavigate(
            ListMovieFragmentDirections.actionListMovieFragmentToBottomSheetDescriptionFragment(
                movie
            )
        )
    }

    private fun NavController.safeNavigate(direction: NavDirections) {
        currentDestination?.getAction(direction.actionId)?.run {
            navigate(direction)
        }
    }
}