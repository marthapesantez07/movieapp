package com.rappi.android.movieapp

import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.rappi.android.movieapp.di.AppComponent
import com.rappi.android.movieapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import java.io.File


class ApplicationClass : DaggerApplication() {


    companion object {
        lateinit var appComponent: AppComponent

        //is used as cached for exoplayer
        lateinit var simpleCache: Cache

        //desaloja los archivos para evitar que se llene el cache
        // var leastRecentlyUsedCacheEvictor: LeastRecentlyUsedCacheEvictor? = null

        //es usado como una bd para almacenar los indices de our cached data
        //var exoDatabaseProvider: ExoDatabaseProvider? = null
        var exoPlayerCacheSize: Long = 1024 * 1024 * 1024 //1gb cache
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

    //create an instance of cache to use in the whole application
    override fun onCreate() {
        super.onCreate()
        val exoCacheDir = File("${cacheDir.absolutePath}/exo")
        val leastRecentlyUsedCacheEvictor = LeastRecentlyUsedCacheEvictor(exoPlayerCacheSize)
        val exoDatabaseProvider = ExoDatabaseProvider(this)
        simpleCache = SimpleCache(exoCacheDir, leastRecentlyUsedCacheEvictor, exoDatabaseProvider)
    }

}