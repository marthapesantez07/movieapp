package com.rappi.android.movieapp.modules.listMovie.view.adapter

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.rappi.android.movieapp.modules.listMovie.view.holder.LoaderViewHolder
import javax.inject.Inject

class LoaderViewAdapter @Inject constructor(private val retry: () -> Unit) :
    LoadStateAdapter<LoaderViewHolder>() {
    override fun onBindViewHolder(holder: LoaderViewHolder, loadState: LoadState) {
        holder.binding(retry, loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoaderViewHolder {
        return LoaderViewHolder.createViewHolder(parent)
    }
}