package com.rappi.android.movieapp.modules.descriptionMovie.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rappi.android.data.rx.SchedulerProvider
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.models.VideoResultModel
import com.rappi.android.domain.usecases.movies.GetVideoMovie
import com.rappi.android.movieapp.modules.descriptionMovie.view.BottomSheetDescriptionFragmentArgs
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class BottomSheetDescriptionViewModel @Inject constructor(
    private val getVideoMovie: GetVideoMovie,
    private val schedulerProvider: SchedulerProvider
) :
    ViewModel() {

    private val _resultMovie: MutableLiveData<MovieResultModel> = MutableLiveData()

    val resultMovie: LiveData<MovieResultModel>
        get() = _resultMovie

    private val _resultVideo: MutableLiveData<VideoResultModel> = MutableLiveData()
    val resultVideo: LiveData<VideoResultModel>
        get() = _resultVideo

    private val compositeDisposable = CompositeDisposable()

    fun setUp(bundle: BottomSheetDescriptionFragmentArgs) {
        bundle.movieResult.let {
            _resultMovie.value = it
            getVideoMovie(it.id ?: 0)
        }
    }

    private fun getVideoMovie(movieId: Int) {
        getVideoMovie.invoke(movieId)
            .compose(schedulerProvider.getSchedulersForMaybe())
            .subscribe({
                Log.i("BottomSheet", "getVideoMovie ->  $it")
                _resultVideo.postValue(it.results.firstOrNull()) //get first video
            }, {
                Log.e("BottomSheet", "getVideoMovie -> error $it")
            }, {
                Log.i("BottomSheet", "getVideoMovie -> data not found")
                //not found - get online
            }).let {
                compositeDisposable.add(it)
            }
        if (_resultVideo.value == null) {
            getVideoOnline(movieId)
        }
    }

    private fun getVideoOnline(movieId: Int) {
        getVideoMovie.invokeOnline(movieId)
            .compose(schedulerProvider.getSchedulersForSingle())
            .subscribe({
                Log.i("BottomSheet", "getVideoMovieOnlne ->  $it")
                _resultVideo.postValue(it.results.firstOrNull()) //get first video
            }, {
                Log.e("BottomSheet", "getVideoMovieOnline -> error $it")
            }).let {
                compositeDisposable.add(it)
            }
    }

    fun getVideo(): VideoResultModel? {
        return _resultVideo.value
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}