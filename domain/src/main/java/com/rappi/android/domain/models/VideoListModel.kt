package com.rappi.android.domain.models

data class VideoListModel(
    val idMovie: Int,
    val results: MutableList<VideoResultModel>
)