package com.rappi.android.domain.usecases.movies

import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.repository.MovieRepo
import com.rappi.android.domain.repository.VideoMovieRepo
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class GetVideoMovie @Inject constructor(private val videoMovieRepo: VideoMovieRepo) {
    fun invoke(movieId: Int): Maybe<VideoListModel> = videoMovieRepo.getVideoByMovieId(movieId)
    fun invokeOnline(movieId: Int): Single<VideoListModel> =
        videoMovieRepo.getVideoByMovieOnline(movieId)
}