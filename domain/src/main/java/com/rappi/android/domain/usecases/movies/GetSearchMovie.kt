package com.rappi.android.domain.usecases.movies

import com.rappi.android.domain.enums.MovieCategory
import com.rappi.android.domain.repository.MovieRepo
import javax.inject.Inject

class GetSearchMovie @Inject constructor(private val movieRepo: MovieRepo) {
    fun invokePaging(query: String) =
        movieRepo.getSearchMoviePaging(MovieCategory.SEARCH.name, query)
}