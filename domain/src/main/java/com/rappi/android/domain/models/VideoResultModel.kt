package com.rappi.android.domain.models

data class VideoResultModel(
    val iso6391: String,
    val iso31661: String,
    val name: String,
    val key: String,
    val site: String,
    val size: String,
    val type: String,
    val official: Boolean,
    val publishedAt: String,
    val id: String
)