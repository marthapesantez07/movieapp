package com.rappi.android.domain.models

class MovieListModel(
    val movieList: Long? = 0,
    val page: Int? = 1,
    val results: List<MovieResultModel>?=null,
    val totalResults: Int? = null,
    val totalPages: Int? = null,
    val nextKey: Int? = null,
    val prevKey: Int? = null
)