package com.rappi.android.domain.networkObjects

import com.google.gson.annotations.SerializedName

data class MovieList(
    @SerializedName("page")
    val page: Int = 1,
    @SerializedName("results")
    val results: List<MovieResult>,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("total_pages")
    val totalPages: Int
)
