package com.rappi.android.domain.repository

import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.MovieResult
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface VideoMovieRepo {
    fun getVideoByMovieId(movieId: Int): Maybe<VideoListModel>
    fun refreshVideoByMovie(listMovies: List<MovieResult>): Completable
    fun getVideoByMovieOnline(movieId: Int): Single<VideoListModel>
}