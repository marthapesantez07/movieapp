package com.rappi.android.domain.enums

enum class MovieCategory {
    POPULAR,
    TOP_RATED,
    SEARCH,
    UNKNOWN;

    companion object {
        fun getCategory(value: String): MovieCategory {
            return values().firstOrNull { it.name == value } ?: UNKNOWN
        }
    }
}