package com.rappi.android.domain.datasource.local

import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Completable
import io.reactivex.Maybe

interface VideoMovieLocalDataSource {
    fun getVideoByMovieIdLocal(movieId: Int): Maybe<VideoListModel>
    fun saveVideoByMovie(movieId: Int, videoList: VideoList): Completable
}