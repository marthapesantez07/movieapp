package com.rappi.android.domain.models

import java.io.Serializable


data class MovieResultModel(
    val idMovieList: Long?=null,
    val posterPath: String? = null,
    val adult: Boolean? = null,
    val overview: String? = null,
    val releaseDate: String? = null,
    val id: Int? = null,
    val originalTitle: String? = null,
    val originalLanguage: String? = null,
    val title: String? = null,
    val backdropPath: String? = null,
    val popularity: Double? = null,
    val voteCount: Int? = null,
    val video: Boolean? = null,
    val voteAverage: Double? = null
) : Serializable