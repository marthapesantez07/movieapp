package com.rappi.android.domain.datasource.remote

import com.rappi.android.domain.networkObjects.MovieList
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Single

interface MovieNetworkDatasource {

    //paging
    suspend fun getPopularMoviePaging(page: Int): MovieList?
    suspend fun getTopRatePaging(page: Int): MovieList?
    suspend fun getSearchMoviePaging(query: String, page: Int): MovieList?
}