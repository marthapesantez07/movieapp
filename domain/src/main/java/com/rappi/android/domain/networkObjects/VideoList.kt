package com.rappi.android.domain.networkObjects

data class VideoList(
    val id: Int,
    val results: List<VideoResult>
    )