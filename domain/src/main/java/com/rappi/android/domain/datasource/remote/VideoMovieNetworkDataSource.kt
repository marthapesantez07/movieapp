package com.rappi.android.domain.datasource.remote

import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Single

interface VideoMovieNetworkDataSource {
    fun getVideoByMovieId(movieId: Int): Single<VideoList>
}