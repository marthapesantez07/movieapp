package com.rappi.android.domain.datasource.local

import androidx.paging.LoadType
import com.rappi.android.domain.models.MovieListModel
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.MovieList
import com.rappi.android.domain.networkObjects.VideoList
import io.reactivex.Completable
import io.reactivex.Maybe

interface MovieLocalDataSource {
    //paging
    suspend fun saveMoviePaging(
        movieCategory: String,
        movieList: MovieList,
        loadTye: LoadType,
        page: Int,
        endOfPaginationReached: Boolean
    )
    suspend fun getRemoteKeys(movieId: Int): MovieListModel?
}