package com.rappi.android.domain.repository

import androidx.paging.PagingData
import com.rappi.android.domain.models.MovieResultModel
import com.rappi.android.domain.models.VideoListModel
import com.rappi.android.domain.networkObjects.MovieResult
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import kotlinx.coroutines.flow.Flow

interface MovieRepo {
    //paging
    fun getMoviePaging(category: String): Flow<PagingData<MovieResultModel>>
    fun getSearchMoviePaging(category: String, query: String): Flow<PagingData<MovieResultModel>>
}