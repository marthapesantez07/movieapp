package com.rappi.android.domain.usecases.movies

import com.rappi.android.domain.enums.MovieCategory
import com.rappi.android.domain.repository.MovieRepo
import javax.inject.Inject

class GetPopularMovie @Inject constructor(private val movieRepo: MovieRepo) {
    fun invokePaging() = movieRepo.getMoviePaging(MovieCategory.POPULAR.name)
}